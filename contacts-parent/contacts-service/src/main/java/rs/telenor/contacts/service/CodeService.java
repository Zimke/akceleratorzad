package rs.telenor.contacts.service;

import java.util.List;

import rs.telenor.contacts.model.code.AbstractCode;

public interface CodeService<T extends AbstractCode> {

	public List<T> fetchAll(final Class<T> p_type);

}
