package rs.telenor.contacts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = { "rs.telenor.contacts" })
@EnableJpaRepositories
public class SbApp {

	public static void main(final String[] args) {
		SpringApplication.run(SbApp.class, args);
	}
}
