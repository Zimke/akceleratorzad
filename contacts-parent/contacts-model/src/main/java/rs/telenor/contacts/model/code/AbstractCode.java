package rs.telenor.contacts.model.code;

import javax.persistence.Column;

public abstract class AbstractCode {

	protected Long id;
	protected String name;

	public abstract Long getId();

	public abstract void setId(final Long id);

	@Column(unique = true)
	public abstract String getName();

	public abstract void setName(final String name);

}
