package rs.telenor.contacts.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import rs.telenor.contacts.model.code.AbstractEntity;

@Entity
public class Contact extends AbstractEntity {

	private String firstName;
	private String lastName;
	private String phone;
	private String email;
	private Sex sex;
	private Address address;

	@Id
	@Column(name = "id")
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(final Long id) {
		this.id = id;
	}

	@Column(name = "first_name")
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "last_name")
	public String getLastName() {
		return lastName;
	}

	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}

	@Column(name = "phone")
	public String getPhone() {
		return phone;
	}

	public void setPhone(final String phone) {
		this.phone = phone;
	}

	@Column(name = "email")
	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	@ManyToOne(targetEntity = Sex.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "sex_id")
	public Sex getSex() {
		return sex;
	}

	public void setSex(final Sex sex) {
		this.sex = sex;
	}

	@ManyToOne(targetEntity = Address.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "address_id")
	public Address getAddress() {
		return address;
	}

	public void setAddress(final Address address) {
		this.address = address;
	}

}
