package rs.telenor.contacts.model.code;

public abstract class AbstractEntity {

	protected Long id;

	public abstract Long getId();

	public abstract void setId(final Long id);

}
