package rs.telenor.contacts.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import rs.telenor.contacts.model.code.AbstractCode;

@Entity
public class Sex extends AbstractCode {

	@Id
	@Column(name = "id")
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(final Long id) {
		this.id = id;
	}

	@Column(name = "name", unique = true)
	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(final String name) {
		this.name = name;
	}

}
