package rs.telenor.contacts.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import rs.telenor.contacts.model.code.AbstractCode;

@Entity
public class Country extends AbstractCode {

	private String alpha2;
	private String alpha3;

	@Id
	@Column(name = "id")
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(final Long id) {
		this.id = id;
	}

	@Column(name = "name", unique = true)
	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(final String name) {
		this.name = name;
	}

	@Column(name = "alpha_2", unique = true)
	public String getAlpha2() {
		return alpha2;
	}

	public void setAlpha2(final String alpha2) {
		this.alpha2 = alpha2;
	}

	@Column(name = "alpha_3", unique = true)
	public String getAlpha3() {
		return alpha3;
	}

	public void setAlpha3(final String alpha3) {
		this.alpha3 = alpha3;
	}

}
