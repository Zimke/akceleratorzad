package rs.telenor.contacts.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import rs.telenor.contacts.model.code.AbstractEntity;

@Entity
public class Address extends AbstractEntity {

	private String street;
	private String streetNo;
	private City city;

	@Id
	@Column(name = "id")
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(final Long id) {
		this.id = id;
	}

	@Column(name = "street")
	public String getStreet() {
		return street;
	}

	public void setStreet(final String street) {
		this.street = street;
	}

	@Column(name = "street_no")
	public String getStreetNo() {
		return streetNo;
	}

	public void setStreetNo(final String streetNo) {
		this.streetNo = streetNo;
	}

	@ManyToOne(targetEntity = City.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "city_id")
	public City getCity() {
		return city;
	}

	public void setCity(final City city) {
		this.city = city;
	}

}
